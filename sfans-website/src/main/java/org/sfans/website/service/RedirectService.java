package org.sfans.website.service;

import java.util.Optional;

import org.sfans.core.domain.Redirect;
import org.sfans.core.domain.Website;

public interface RedirectService {

	Optional<Redirect> findRedirect(Website website, String url);
}
