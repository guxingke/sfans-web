package org.sfans.common.utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class CsvExcelUtils {
	private static final String[] CSV_EXCEL_SUFFIXES = { "csv", "xls", "xlsx" };

	/**
	 * 是否是csv或excel文件
	 * 
	 * @param filename
	 * @return
	 */
	public static boolean isCsvExcel(String filename) {
		if (StringUtils.isEmpty(filename))
			return false;
		return ArrayUtils.contains(CSV_EXCEL_SUFFIXES, FilenameUtils.getExtension(filename).toLowerCase());
	}

}
