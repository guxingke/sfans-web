package org.sfans.core.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.BatchSize;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({ "version" })
public class Website {

	public static enum Status {
		ENABLED, DISABLED, PENDING_REMOVE
	}

	@Id
	@GeneratedValue
	private long id;

	@Column(length = 100)
	private String name;
	private String description;

	private Status status = Status.ENABLED;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;
	@Temporal(TemporalType.TIMESTAMP)
	private Date deleteRequestedAt;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "website", cascade = CascadeType.ALL, orphanRemoval = true)
	@BatchSize(size = 10)
	private final Set<Host> hosts = new HashSet<>();

	@Version
	private long version;

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(final Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(final Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(final long version) {
		this.version = version;
	}

	public Set<String> getHosts() {
		return hosts.stream().map(Host::getName).collect(Collectors.toSet());
	}

	public void setHosts(final Set<String> hosts) {
		this.hosts.removeIf(host -> !hosts.contains(host.getName()));
		this.hosts.addAll(hosts.stream().map(Host::new).collect(Collectors.toSet()));
		this.hosts.forEach(host -> host.setWebsite(this));
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(final Status status) {
		this.status = status;
	}

	public Date getDeleteRequestedAt() {
		return deleteRequestedAt;
	}

	public void setDeleteRequestedAt(final Date deleteRequestedAt) {
		this.deleteRequestedAt = deleteRequestedAt;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE).append("id", id)
				.append("name", name).append("hosts", hosts).toString();
	}

}
