package org.sfans.core.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3096465954722121152L;

	public ResourceNotFoundException(final String message) {
		super(message);
	}

	public ResourceNotFoundException() {
		super();
	}
}
