package org.sfans.admin.controller.api;

import org.sfans.core.domain.PostRepository;
import org.sfans.core.domain.UserRepository;
import org.sfans.core.domain.WebsiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Profile("admin")
@RestController("adminApiController")
@RequestMapping("${sfans.admin.urls.api}/admin")
@Secured("ROLE_ADMIN")
public class AdminController {

	@Autowired
	private UserRepository users;

	@Autowired
	private WebsiteRepository websites;

	@Autowired
	private PostRepository posts;

	@RequestMapping("statistics")
	public Statistics statistics() {
		Statistics stats = new Statistics();
		stats.setUsers(users.count());
		stats.setWebsites(websites.count());
		stats.setPages(posts.count());

		return stats;
	}

	@SuppressWarnings("unused")
	private static class Statistics {
		private long users;
		private long pages;
		private long websites;

		public long getUsers() {
			return users;
		}

		public void setUsers(final long users) {
			this.users = users;
		}

		public long getPages() {
			return pages;
		}

		public void setPages(final long pages) {
			this.pages = pages;
		}

		public long getWebsites() {
			return websites;
		}

		public void setWebsites(final long websites) {
			this.websites = websites;
		}

	}
}
