package org.sfans.admin.domain;

public enum Roles {
	ROLE_USER, ROLE_ADMIN, ROLE_API
}
